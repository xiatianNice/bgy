function _resetFrameHeight() {
    var a = this.frameElement;
    a && (a.height = a.contentDocument ? a.contentDocument.documentElement.scrollHeight + 0 : "MSIE9.0" == _browser_Version ? a.document.documentElement.scrollHeight : a.contentWindow.document.body.scrollHeight + 0, _heightSeted = !0);
    window != window.top && window.parent._resetFrameHeight()
}

$(function(){
    _resetFrameHeight();
});

// 草稿箱
var draftPlugins = Vue.extend({
    template: '<el-dialog title="草稿箱" :visible.sync="draftDialogVisible" width="700px">' +
        '<el-table :data="draftList" border style="width: 100%" max-height="480">' +
        '<el-table-column prop="area" label="区域" type="index" width="50" header-align="center" align="center"></el-table-column>' +
        '<el-table-column prop="name" label="项目名称" header-align="center" align="center"></el-table-column>' +
        '<el-table-column label="操作" width="180" header-align="center" align="center">' +
        '<template slot-scope="scope">' +
        '<el-button type="text">编辑</el-button>' +
        '<el-button type="text">删除</el-button>' +
        '</template>' +
        '</el-table-column>' +
        '</el-table>' +
        '<el-pagination class="mt20 tc" background @size-change="handleSizeChange" @current-change="handleNoChange" :current-page="query.pageNo" :page-size="query.pageSize" layout="total, prev, pager, next, jumper" :total="query.totalSize">' +
        '</el-pagination>' +
        '</el-dialog>',
    data() {
        return {
            draftDialogVisible: true,
            query: {
                pageNo: 1,
                pageSize: 5,
                totalSize: 0
            },
            draftList: [],
        }
    },
    mounted () {
        this.fetchDraftList();
    },
    methods: {
        fetchDraftList () {
            // ajax请求
            var dataJson = {
                draftList: [
                    {
                        area: '广清区域',
                        name: '碧桂园太阳城二期九区'
                    },
                    {
                        area: '广清区域',
                        name: '碧桂园太阳城二期九区'
                    },
                    {
                        area: '广清区域',
                        name: '碧桂园太阳城二期九区'
                    },
                    {
                        area: '广清区域',
                        name: '碧桂园太阳城二期九区'
                    },
                    {
                        area: '广清区域',
                        name: '碧桂园太阳城二期九区'
                    },
                ],
                pageNo: 1,
                pageSize: 5,
                totalSize: 22,
            };
            this.draftList = dataJson.draftList || [];
            this.query.pageNo = dataJson.pageNo;
            this.query.pageSize = dataJson.pageSize;
            this.query.totalSize = dataJson.totalSize;
        },
        handleSizeChange (pageSize) {
            this.query.pageSize = pageSize;
            this.fetchDraftList();
        },
        handleNoChange (pageNo) {
            this.query.pageNo = pageNo;
            this.fetchDraftList();
        }
    }
});



